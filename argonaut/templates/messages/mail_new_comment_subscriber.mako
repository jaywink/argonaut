<p>Hello,</p>
<p>There is a new comment for the post '${post_subject}' at ${site_name}.</p>
<p>To see the comment, please click the following link: <a href='${post_url}'>${post_url}</a></p>
<p>If you want to unsubscribe from these notifications, please click this link: <a href='${unsubscribe_url}'>${unsubscribe_url}</a></p>
<p>If you feel this notification has reached you in error, please forward this message to the admin (${admin_email}) and let him know.</p>
<p>Best regards,<br>The Argonaut mailsender</p>
