"""The comment model"""
from sqlalchemy import Column, ForeignKey
from sqlalchemy.types import Integer, Unicode, UnicodeText, Date

from argonaut.model.meta import Base, Session

class Comment(Base):
    __tablename__ = 'comment'

    id   = Column(Integer, primary_key=True)
    post_id = Column(Integer, ForeignKey('post.id'), nullable=False)
    body = Column(UnicodeText, nullable=False)
    posted = Column(Date, nullable=False)
    author = Column(Unicode(50), nullable=True)
    author_website = Column(Unicode(300), nullable=True)
    author_email = Column(Unicode(300), nullable=True)
    author_subscribe = Column(Unicode(1), nullable=True)
    author_unsubscribe_url = Column(Unicode(300), nullable=True)

    def __init__(self, id=None,post_id=None,body=None,posted=None,author=None,author_website=None,author_email=None,author_subscribe=None,author_unsubscribe_url=None):
        self.id = id
        self.post_id = post_id
        self.body = body
        self.posted = posted
        self.author = author
        self.author_website = author_website
        self.author_email = author_email
        self.author_subscribe = author_subscribe
        self.author_unsubscribe_url = author_unsubscribe_url

    def __unicode__(self):
        return self.body
        
    def __repr__(self):
        return "<Comment('%s','%s', '%s', '%s', '%s', '%s')>" % (self.id,self.post_id,self.body,self.posted,self.author,self.author_website)

    __str__ = __unicode__
    
def new():
    return Comment()    
    
def save(comment):
    Session.add(comment)
    Session.commit()
        
def get_post_comments(post_id):
    return Session.query(Comment).filter_by(post_id=post_id).all()
    
def get_post_comment_count(id):
    return Session.query(Comment).filter_by(post_id=id).count()    
    
def get_subscriber_emails(id):
    return Session.query(Comment).filter_by(post_id=id).filter(Comment.author_subscribe=='1').all()
    
def remove_subscribtion(id, uuid):
    comment = Session.query(Comment).filter_by(post_id=id).filter(Comment.author_unsubscribe_url.contains(uuid)).first()
    if comment:
        comment.author_subscribe = '0'
        Session.commit()
        return True
    else:
        return False
    
