--0.3.6 - 0.4.0
alter table comment add author_email varchar(300) null;
alter table comment add author_subscribe char(1) null;
alter table comment add author_unsubscribe_url varchar(300) null;

--0.3.4 - 0.3.5
ALTER TABLE `pages` ADD `parent_id` INT NULL;
